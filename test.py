#!/usr/bin/env python2
import sys, random
from untwist import *

## something you don't know
seed = 0x1337
r    = random.Random(seed)
blob = [r.getrandbits(32) for _ in range(624)]

predicted = []
mt = [revert(x) for x in blob]
for i in range(N):
    twist(mt, i)
    predicted.append(extract(mt[i]))

assert predicted[0] == r.getrandbits(32)
