import sys
import random
N = 624
M = 397
A = 2567483615
B = 2636928640
C = 4022730752
R = 31
MASK_LOWER = (1 << R) - 1
MASK_UPPER = (1 << R)

## https://en.wikipedia.org/wiki/Mersenne_Twister#C.2FC.2B.2B_implementation
def extract(y):
    y ^= y >> 11
    y ^= y << 7  & B
    y ^= y << 15 & C
    y ^= y >> 18
    return y

def twist(mt, i):
    x = mt[i] & MASK_UPPER | mt[(i + 1) % N] & MASK_LOWER
    xA = x >> 1
    if x & 1:
        xA ^= A
    mt[i] = mt[(i + M) % N] ^ xA

def revert(y):
    y ^= y >> 18
    y ^= y << 15 & C

    T = y
    y ^= (T <<  7 & B)
    y ^= (T << 14 & 0x94284000)
    y ^= (T << 21 & 0x14200000)
    y ^= (T << 28 & (1 << 28))

    T = y
    y ^= T >> 11
    y ^= T >> 22
    return y
