#!/bin/sh
## generate 10000 random bytes
python -c $'import random\nr = random.Random()\nfor i in range(10000): print(r.getrandbits(32))' > blob

head -n 624 blob > known
tail -n '+625' blob > unknown
