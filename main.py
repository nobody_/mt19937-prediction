#!/usr/bin/env python2
import sys
from untwist import *

## how many values you need
need = int(sys.argv[2]) if len(sys.argv) > 2 else (1000 - N)
got  = 0

blob_path = sys.argv[1] if len(sys.argv) > 1 else './known'
blob      = open(blob_path, 'r').read().splitlines()

## sanity checks
if len(blob) < N:
    print('Please provide at least %u numbers' % (N))
    sys.exit(1)

for i, b in enumerate(blob):
    x = int(b)
    if not (0 <= x <= (1<<32)):
        print('Bad value [%s] at %u line' % (b, i))
        sys.exit(1)
    blob[i] = x

## throw away redundant known values
blob = blob[:N]
predicted = []

while got <= need:
    try:
        current
    except:
        current = blob

    mt = [revert(x) for x in current]
    for i in range(N):
        twist(mt, i)
        predicted.append(extract(mt[i]))
    current = predicted[-N:]
    got += N

predicted = predicted[:need]
print('\n'.join([str(x) for x in predicted]))
